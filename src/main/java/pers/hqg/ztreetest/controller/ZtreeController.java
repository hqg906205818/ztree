package pers.hqg.ztreetest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pers.hqg.ztreetest.dao.RoleDao;
import pers.hqg.ztreetest.entity.Role;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/ztree")
public class ZtreeController {

    @Autowired
    RoleDao roleDao;


    @GetMapping("/Role")
    public String RoleList(){
        return "Role.html";
    }

    @GetMapping("/Index")
    public String Index(Integer roleid,Model model){
        Optional<Role> role=roleDao.findById(roleid);
        model.addAttribute("roleid",role.get().getRoleid());
        model.addAttribute("rolename",role.get().getRolename());
        return "Index.html";
    }

    @RequestMapping("/getRole")
    public String getAllRole(Model model){
        List<Role> roleList=roleDao.findAll();
        model.addAttribute("role",roleList);
        return "Role";
    }

    @RequestMapping("/addPermission2Role")
    public Role addPermissionToRole(Integer roleid){
        Optional<Role> role=roleDao.findById(roleid);
        return role.get();
    }
}
