package pers.hqg.ztreetest.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pers.hqg.ztreetest.entity.Role;

public interface RoleDao extends JpaRepository<Role,Integer> {
}
