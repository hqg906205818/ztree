package pers.hqg.ztreetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZtreetestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZtreetestApplication.class, args);
    }

}
